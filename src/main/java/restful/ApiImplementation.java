package restful;

import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright Dr. Ganesh R. Baliga
 * All rights reserved.
 */
public class ApiImplementation extends Api {

    Logger logger = LoggerFactory.getLogger(ApiImplementation.class);

    Sql2o sql2o;
    String db_jdbc_url, db_user, db_password;

    final String DBInfoFile = "DB_INFO"; // Text file containing db credentials

    // Read database connection information from credentials file
    // Credentials file format:
    // DB_JDBC_URL=jdbc:mysql://your_msql_dbserver.com/your_database
    // DB_USER=your_db_username
    // DB_PASSWORD=your_db_password

    boolean init() {
        try {
            FileReader fr = new FileReader(DBInfoFile);
            BufferedReader reader = new BufferedReader(fr);

            String line;
            while ((line = reader.readLine()) != null) {
                String [] fields = line.split("=");
                if (fields.length == 2) {
                    if (fields[0].equals("DB_JDBC_URL"))
                        db_jdbc_url = fields[1];
                    else if (fields[0].equals("DB_USER"))
                        db_user = fields[1];
                    else if (fields[0].equals("DB_PASSWORD"))
                        db_password = fields[1];
                }
            }
        }
        catch (Exception e) {
            return false;
        }

        return true;

    }

    public ApiImplementation() {

        if (!init()) {
            System.err.println("Unable to read database information");
            System.exit(0);
        }

        sql2o = new Sql2o(db_jdbc_url, db_user, db_password);

    }


    @Override
    public List<City> getCities(String country) {
        try (Connection conn = sql2o.open()) {
            List<City> cities =
                    conn.createQuery("select city.name, city.population from city, country where "
                            + "city.countrycode=country.code and country.name=:countryName "
                            + "order by city.population desc;")
                            .addParameter("countryName", country)
                            .executeAndFetch(City.class);
            return cities;
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }

        return null;
    }

    @Override
    public boolean createCity (
            String name,
            String countryName,
            String district,
            int population
    ) {
        try (Connection conn = sql2o.open()) {
            List<String> codes =
                    conn.createQuery("select code from country where name=:countryName")
                            .addParameter("countryName", countryName)
                            .executeAndFetch(String.class);
            if (codes.size() != 1) {
                return false;
            }
            else {
                String code = codes.get(0);
                conn.createQuery("insert into city (name, countrycode, district, population) "
                        + "values (:name, :code, :district, :population);")
                        .addParameter("name", name)
                        .addParameter("code", code)
                        .addParameter("district", district)
                        .addParameter("population", population)
                        .executeUpdate();
                return true;

            }
        }
        catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }


}
