package restful;

/**
 * Copyright Dr. Ganesh R. Baliga
 * All rights reserved.
 */

/**
 * City is a model class. It models a city in a country
 */
public class City {


    String name;
    int population;

    public City(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }




}
