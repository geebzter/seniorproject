/**
 * Created by Dr. Baliga on 2/11/17.
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import restful.Api;
import restful.City;

import java.util.List;

import static spark.Spark.*;


public class Main {
    static Logger logger = LoggerFactory.getLogger(Main.class);


    public static void main(String[] args) {

        // Serve from port 8080 (instead of 4567)

        port(8080);

        // Location of the static html files
        staticFiles.location("/public");

        // Uncomment below line if webserver is different from
        // the server hosting the restapi. Refer CORS (cross origin resource sharing)

        //after(new CorsEnabler());

        get("/hello", (req, res) -> {
            logger.debug("Get request: /hello");
            return "Hello World";
        });

        get("/cities/:country", (req, res) -> {
            String country = req.params("country");
            logger.info("Get request: /cities"
                    + " for country"
                    + country);
            Api myapi = Api.getApi();
            List<City> cities = myapi.getCities(country);

            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            return gson.toJson(cities);

        });


        post("/city", (request, response) -> {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            response.type("application/json");

            try {
                JsonObject obj = new JsonParser().parse(request.body()).getAsJsonObject();

                String cityName = obj.get("name").getAsString();
                String countryName = obj.get("country").getAsString();
                String district = obj.get("district").getAsString();
                int population = obj.get("population").getAsInt();

                Api myapi = Api.getApi();


                if (myapi.createCity(cityName,countryName,district,population)) {
                    response.status(200);
                    return gson.toJson(obj);
                }
                else {
                    response.status(400);
                    return(null);
                }

            } catch (Exception e) {
                response.status(404);
                return (gson.toJson(e));
            }

        });



    }
}